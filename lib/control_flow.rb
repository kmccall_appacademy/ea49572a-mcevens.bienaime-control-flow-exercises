# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.split("").select do |el|
  el.downcase != el
end.join("")
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
 if str.length % 2 == 0
   str[str.length/2 -1..str.length/2]
 else
   str[str.length/2]

 end

end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.split("").count {|el| VOWELS.include?(el) }
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
 result = 1

 (1..num).each do |el|
   result = result * el
 end
 result
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ""
  arr.each do |el|
    if result == ""
      result = el
    else
      result += separator + el
    end
  end
  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  arr = str.split("")
  arr.each_with_index do |el,idx|
      if idx % 2 != 0
       arr[idx] =   el.upcase
      else
       arr[idx] =   el.downcase
      end

    end.join("")
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  arr = str.split(" ")
  arr.each_with_index do |el,idx|
    if el.length >= 5
      arr[idx] =  el.reverse
    else
      arr[idx] = el
    end

  end
 arr.join(" ")

end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).to_a.each.map do |el|
    if el % 3 == 0 and el % 5 == 0
      el = "fizzbuzz"
    elsif el % 5 == 0
      el = "buzz"
    elsif el % 3 ==0
        el = "fizz"
    else
        el = el
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
result = []

result = arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2..num-1).each do |el|
    if num % el == 0
      return false
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select do |el|
     num % el == 0
  end
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select do  |el|
    prime?(el)
 end
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  factors(num).count do |el|
    prime?(el)
 end
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even_list= []
  odd_list = []

  arr.each do |el|
    if el % 2 == 0
      even_list.push(el)
    else
      odd_list.push(el)
    end
  end

  if even_list.length == 1
    return even_list[0]
  end
  odd_list[0]
end
